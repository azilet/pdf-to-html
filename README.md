# pdf-to-html

## Sample Text Extraction

This is a demo of text extraction from PDF file. Our source PDF file is a
[Java Language Specification (Java SE 8)](https://docs.oracle.com/javase/specs/jls/se8/jls8.pdf)
which is freely available on Internet.

Extracted texts are divided page by page and an [index](html/index.html) file contains links to every page.
For the purpose of demo, only a few dozen of pages are included.


